addEventListener("submit", (event) => {
    event.target.querySelectorAll("form button").forEach(node => { node.disabled = true })
})
addEventListener("htmx:afterOnLoad", (event) => {
    event.target.querySelectorAll("form button").forEach(node => { node.disabled = false })
})

$(function() {
    twemoji.parse(document.body, {
        folder: 'svg',
        ext: '.svg'
    });
    $(`#header__btn-register`).on("click",function (){
        window.location.replace("/register");
    });
    $(`#header__btn-login`).on("click",function (){
        window.location.replace("/login");
    });
    $(`#header__btn-logout`).on("click",function (){
        window.location.replace("/logout");
    });
    $(`#header__btn-submit`).on("click",function (){
        window.location.replace("/submit");
    });
    /*$(`.story__upvote`).on("click",function (){
        htmx.ajax('POST', `${twig_vars.post_url}/upvote`, {
            headers: {
                'HX-Request': 'POST' // Set the header to indicate a POST request
            },
            handler: function (event,data){
                console.log(event)
            },
            source: event.target,
            target: "this",
            swap: "outerHTML"
        });
    })*/
    $('.story__upvote,.story__downvote').on('htmx:beforeSwap', function(event){
        if(event.detail.serverResponse == "vote_exists"){
            $(this).addClass("active");
        }
    })
});